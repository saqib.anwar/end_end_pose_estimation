import torch
import numpy as np
import pickle
import json
import torch.nn as nn
import torch.optim as optim
from loss import Loss_class
import pandas as pd
import matplotlib.pyplot as plt
from torch.autograd import Variable
import time


from dataset_grid_cell_classification import Data_depth as depth_data
from model_4 import Resnet_pose_prediction as neural_net
from utils import Weights_Save
from loss_3 import Loss_class

load_checkpoint = False

if load_checkpoint:
    check_path = ''  # add path to the checkpoint to resume training
    load_point = torch.load(check_path)

model = neural_net()
#model.load_state_dict(load_point['state_dict'])
model = nn.DataParallel(model)
model.cuda()

def pickle_load(path):
    return pickle.load(open(path, 'rb'))

data_path_virtual = '/home/thesis_main_folder/dfusion/data_cleaning/wo_distractors_filtered_files_429893.pickle'
virtual_data_list = pickle_load(data_path_virtual)

train_set_list = virtual_data_list[:200000]
validation_set_list = virtual_data_list[200000:240000]

batch_size_chosen = 10

train_dataset = depth_data(train_set_list)
train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size_chosen, shuffle=True, num_workers=8)

validation_dataset = depth_data(validation_set_list, augmentation= False)
validation_loader = torch.utils.data.DataLoader(validation_dataset, batch_size=batch_size_chosen, shuffle=True, num_workers=8)

criterion = Loss_class()

lr = 0.0001
optimizer = optim.Adam(model.parameters(), lr= lr, weight_decay= 1e-5)
#optimizer = load_point['optimizer']
#optimizer.load_state_dict(load_point['optimizer_state_dict'])
#for param_group in optimizer.param_groups:
# param_group['lr'] = lr
#criterion = Loss_class()

best_error = np.inf 
train_epoch_error_list = []
train_epoch_classification_loss_list = []
train_epoch_x_loss_list = []
train_epoch_y_loss_list = []
train_epoch_z_loss_list = []
train_epoch_q1_loss_list = []
train_epoch_q2_loss_list = []
train_epoch_q3_loss_list = []
train_epoch_q4_loss_list = []
validataion_epoch_error_list = []

store_train_batch_error = []
store_train_batch_classification_loss = []
store_train_batch_x_loss = []
store_train_batch_y_loss = []
store_train_batch_z_loss = []
store_train_batch_q1_loss = []
store_train_batch_q2_loss = []
store_train_batch_q3_loss = []
store_train_batch_q4_loss = []

epochs_list = []
train_data_len_list = []
test_data_len_list = []
learning_rate_list = []

for epochs in range(300):
        
    train_batch_error = []
    train_batch_classification_loss = []
    train_batch_x_loss = []
    train_batch_y_loss = []
    train_batch_z_loss = []
    train_batch_q1_loss = []
    train_batch_q2_loss = []
    train_batch_q3_loss = []
    train_batch_q4_loss = []
    validation_batch_error = []

    epochs_list.append(epochs)

    model.train()

    for batch, i in enumerate(train_loader):

        optimizer.zero_grad()

        img_tensor, target_tensor, loss_weights = i
        img_tensor, target_tensor, loss_weights = Variable(img_tensor).cuda(), Variable(target_tensor).cuda(), Variable(loss_weights).cuda()
        output = model(img_tensor)
        total_loss, classification_loss, x_loss, y_loss, z_loss, q1_loss, q2_loss, q3_loss, q4_loss = criterion(output, target_tensor, loss_weights)
       
        train_batch_error.append(total_loss.item())
        store_train_batch_error.append(total_loss.item())
        
        train_batch_classification_loss.append(classification_loss.item())
        store_train_batch_classification_loss.append(classification_loss.item())
        
        train_batch_x_loss.append(x_loss.item())
        store_train_batch_x_loss.append(x_loss.item())
        
        train_batch_y_loss.append(y_loss.item())
        store_train_batch_y_loss.append(y_loss.item())
        
        train_batch_z_loss.append(z_loss.item())
        store_train_batch_z_loss.append(z_loss.item())
        
        train_batch_q1_loss.append(q1_loss.item())
        store_train_batch_q1_loss.append(q1_loss.item())
        
        train_batch_q2_loss.append(q2_loss.item())
        store_train_batch_q2_loss.append(q2_loss.item())
        
        train_batch_q3_loss.append(q3_loss.item())
        store_train_batch_q3_loss.append(q3_loss.item())
        
        train_batch_q4_loss.append(q4_loss.item())
        store_train_batch_q4_loss.append(q4_loss.item())

        print('Epoch: {0} Batch: {1} total_loss: {2} class_loss: {3} x_loss: {4} y_loss: {5} z_loss: {6} q1_loss: {7} q2_loss: {8} q3_loss: {9} q4_loss: {10} best_loss: {11}'.format(epochs, batch, total_loss.item(), classification_loss.item(), x_loss.item(), y_loss.item(), z_loss.item(), q1_loss.item(), q2_loss.item(), q3_loss.item(), q4_loss.item(), best_error))
        total_loss.backward()
        optimizer.step()

    train_ave_error = np.mean(np.array(train_batch_error))
    train_epoch_error_list.append(train_ave_error)
    train_epoch_classification_loss_list.append(np.mean(np.array(train_batch_classification_loss)))
    train_epoch_x_loss_list.append(np.mean(np.array(train_batch_x_loss)))
    train_epoch_y_loss_list.append(np.mean(np.array(train_batch_y_loss)))
    train_epoch_z_loss_list.append(np.mean(np.array(train_batch_z_loss)))
    train_epoch_q1_loss_list.append(np.mean(np.array(train_batch_q1_loss)))
    train_epoch_q2_loss_list.append(np.mean(np.array(train_batch_q2_loss)))
    train_epoch_q3_loss_list.append(np.mean(np.array(train_batch_q3_loss)))
    train_epoch_q4_loss_list.append(np.mean(np.array(train_batch_q4_loss)))

    train_data_len_list.append(200000)
    test_data_len_list.append(40000)
    learning_rate_list.append(lr)
    time.sleep(10)
    torch.save(model.state_dict(), 'results/single_wo_distractor/feb_10/pose_prediction_{0}_{1}.pth'.format(epochs, train_ave_error))
    
    model.eval()

    for batch, i in enumerate(validation_loader):
        img_tensor, target_tensor, loss_weights = i
        img_tensor, target_tensor, loss_weights = Variable(img_tensor).cuda(), Variable(target_tensor).cuda(), Variable(loss_weights).cuda()
        output = model(img_tensor)

        validation_loss, classification_loss, _, _, _, _, _, _, _  = criterion(output, target_tensor, loss_weights)       
        validation_batch_error.append(validation_loss.item())

        print('Epoch: {0} Batch: {1} validation_loss: {2} best_loss: {3}'.format(epochs, batch, validation_loss.item(), best_error))
    
    validation_ave_error = np.mean(np.array(validation_batch_error))
    if validation_ave_error < best_error:
        best_error = validation_ave_error
        torch.save(model.state_dict(), 'results/single_wo_distractor/feb_10/pose_prediction_{0}_{1}.pth'.format(epochs, best_error))
        weights_save = Weights_Save(optimizer, model, 'results/single_wo_distractor/feb_10/checkpoint_pose_prediction_{0}_{1}.pth'.format(epochs, best_error))
        weights_save.save_checkpoint()


    validataion_epoch_error_list.append(validation_ave_error)

    data_dict = {'epochs':epochs_list, 'train_loss':train_epoch_error_list, 'train_classification_loss': train_epoch_classification_loss_list, 'train_x_loss':train_epoch_x_loss_list, 'train_y_loss':train_epoch_y_loss_list, 'train_z_loss':train_epoch_z_loss_list, 'train_q1_loss':train_epoch_q1_loss_list, 'train_q2_loss':train_epoch_q2_loss_list, 'train_q3_loss':train_epoch_q3_loss_list, 'train_q4_loss':train_epoch_q4_loss_list, 'validation_loss': validataion_epoch_error_list, 'train_data_len': train_data_len_list, 'validation_data_len': test_data_len_list, 'learning_rate': learning_rate_list}#, 'test_loss':test_loss_list, 'test_dis':test_dis_list}
    df = pd.DataFrame(data= data_dict)
    save_df = df.to_csv('results/single_wo_distractor/feb_10/per_epoch_loss_dataframe.csv', index= False)
    plt.plot(df['epochs'], df['train_loss'], label = 'train_loss')
    plt.plot(df['epochs'], df['validation_loss'], label = 'validation_loss')
    plt.title('Per Epoch Loss Visualization')
    plt.legend()
    plt.savefig('results/single_wo_distractor/feb_10/per_epoch_losses.png')
    plt.close()

# below is training batch dataframe

    batch_dict = {'train_loss':store_train_batch_error, 'train_classification_loss': store_train_batch_classification_loss, 'train_x_loss':store_train_batch_x_loss, 'train_y_loss':store_train_batch_y_loss, 'train_z_loss':store_train_batch_z_loss, 'train_q1_loss':store_train_batch_q1_loss, 'train_q2_loss':store_train_batch_q2_loss, 'train_q3_loss':store_train_batch_q3_loss, 'train_q4_loss':store_train_batch_q4_loss}#, 'test_loss':test_loss_list, 'test_dis':test_dis_list}
    batch_df = pd.DataFrame(data= batch_dict)
    save_df = batch_df.to_csv('results/single_wo_distractor/feb_10/per_batch_loss_dataframe.csv', index= False)