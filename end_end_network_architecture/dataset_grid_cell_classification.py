import torch
import numpy as np
from PIL import Image
import json
import pickle
import torchvision.transforms as transform
from scipy.spatial.transform import Rotation as R


class Data_depth(torch.utils.data.Dataset):
    def __init__(self, datalist, augmentation = True):
        super(Data_depth, self).__init__()
        
        self.datalist = datalist #pickle.load(open(self.datapath, 'rb'))
        self.norm = transform.Normalize(mean=[0.43425, 0.418558, 0.7150218], std=[0.203155, 0.204383, 0.20547928])
        self.augmentation = augmentation
        if self.augmentation == False:
            self.jitter = transform.ColorJitter(brightness=0, contrast=0, saturation=0, hue=0)
        else:
            self.jitter = transform.ColorJitter(brightness=0.3, contrast=0.3, saturation=0.3, hue=0.3)


    def __len__(self):
        return len(self.datalist)
        
    def img_return(self, img):
        #self.img = self.jitter(img)
        self.img = (img)
        img_tensor = transform.ToTensor()(self.img)
        #img_tensor = torch.cat((img_tensor,img_tensor,img_tensor))
        norm_img = self.norm(img_tensor)
        return norm_img

    def center_grid_cell(self, json_file, grid_division = 8):
        
        x_size, y_size = 640/ grid_division, 480/grid_division
        json_data = json.load(open(json_file))
        cx, cy = json_data['objects'][0]['projected_cuboid_centroid'][0], json_data['objects'][0]['projected_cuboid_centroid'][1] 
        grid_x , grid_y = cx//x_size, cy//y_size
        
        return int(grid_x), int(grid_y)

    def center_normalized(self, json_file, grid_division = 8):

        json_data = json.load(open(json_file))
        cx, cy = json_data['objects'][0]['projected_cuboid_centroid'][0], json_data['objects'][0]['projected_cuboid_centroid'][1] 
        norm_x, norm_y = (cx%80), cy%60
        norm_x, norm_y = norm_x/80, norm_y/60
        return norm_x, norm_y

    def pose_quternions(self, json_file):
        rot_matrix = np.array(json.load(open(json_file))['objects'][0]['pose_transform'])[:-1,:-1]
        r = R.from_dcm(rot_matrix)
        return r.as_quat()

    def normalized_z(self, json_file):
        z_val = np.array(json.load(open(json_file))['objects'][0]['pose_transform'])[-1,-2]
        return z_val/150


    def __getitem__(self, index):
        img_path, _, depth_path, json_file, _ = self.datalist[index]
        img_path = img_path.replace('/run/user/1000/gvfs/smb-share:server=149.201.37.175,share=anwar/', '/home/')
        depth_path = depth_path.replace('/run/user/1000/gvfs/smb-share:server=149.201.37.175,share=anwar/', '/home/')
        json_file = json_file.replace('/run/user/1000/gvfs/smb-share:server=149.201.37.175,share=anwar/', '/home/')
        img = Image.open(img_path).convert('RGB')
        self.jitter(img)
        depth = Image.open(depth_path)
        
        img_array = np.array(img)[:,:,:-1]
        depth_array = np.array(depth)
        depth_array_1 = np.expand_dims(depth_array, axis = 2)
        img_combination = np.concatenate((img_array, depth_array_1), axis =2)

        objectness_x, objectness_y = self.center_grid_cell(json_file)
        target_centerx, target_centery = self.center_normalized(json_file)
        z_norm = self.normalized_z(json_file)
        quat_pose = self.pose_quternions(json_file)

        input_tensor = self.img_return(img_combination)

        target_tensor = torch.zeros(8,8,8)
        loss_weights = torch.ones(8,8)/64

        target_tensor[:,objectness_y, objectness_x] = torch.tensor([1, target_centerx, target_centery, z_norm] + list(quat_pose))
        loss_weights[objectness_y, objectness_x] = torch.tensor(63/64)

        return input_tensor, target_tensor, loss_weights
