import torch
import torch.nn as nn
import torch.nn.functional as F
from utils import Posed_model_points
import pickle
from torch.autograd import Variable


class Loss_class(nn.Module):
    def __init__(self):
        super(Loss_class, self).__init__()
                
    def forward(self,  in_tensor, target,loss_weights):

        output = in_tensor.reshape(-1,8,8,8)
        class_preds = torch.sigmoid(output[:,0,:,:])
        grid_classification_loss = F.binary_cross_entropy(class_preds, target[:,0,:,:], loss_weights)#[:,0,:,:])

        x_loss = (output[:,1,:,:] - target[:,1,:,:])**2
        x_loss = x_loss * loss_weights#[:,1,:,:]
        x_loss = x_loss.mean(0)
        x_loss = x_loss.sum()

        y_loss = (output[:,2,:,:] - target[:,2,:,:])**2
        y_loss = y_loss * loss_weights#[:,2,:,:]
        y_loss = y_loss.mean(0)
        y_loss = y_loss.sum()

        z_loss = (output[:,3,:,:] - target[:,3,:,:])**2
        z_loss = z_loss * loss_weights#[:,3,:,:]
        z_loss = z_loss.mean(0)
        z_loss = z_loss.sum()

        q1_loss = (output[:,4,:,:] - target[:,4,:,:])**2
        q1_loss = q1_loss * loss_weights#[:,4,:,:]
        q1_loss = q1_loss.mean(0)
        q1_loss = q1_loss.sum()

        q2_loss = (output[:,5,:,:] - target[:,5,:,:])**2
        q2_loss = q2_loss * loss_weights#[:,5,:,:]
        q2_loss = q2_loss.mean(0)
        q2_loss = q2_loss.sum()

        q3_loss = (output[:,6,:,:] - target[:,6,:,:])**2
        q3_loss = q3_loss * loss_weights#[:,6,:,:]
        q3_loss = q3_loss.mean(0)
        q3_loss = q3_loss.sum()

        q4_loss = (output[:,7,:,:] - target[:,7,:,:])**2
        q4_loss = q4_loss * loss_weights#[:,7,:,:]
        q4_loss = q4_loss.mean(0)
        q4_loss = q4_loss.sum()


        total_loss = grid_classification_loss + x_loss + y_loss + z_loss + q1_loss + q2_loss + q3_loss + q4_loss
        return total_loss, grid_classification_loss.detach().cpu(), x_loss.detach().cpu(), y_loss.detach().cpu(), z_loss.detach().cpu(), q1_loss.detach().cpu(), q2_loss.detach().cpu(), q3_loss.detach().cpu(), q4_loss.detach().cpu()