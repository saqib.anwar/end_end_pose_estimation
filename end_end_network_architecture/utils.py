import numpy as np
import torch 
import time

def meter_prediction(pixel_prediction):
    return pixel_prediction*5

def img_to_depth(depth_img, row, col):
    pixel_val = np.copy(depth_img)[row, col]
    depth_meters = (pixel_val * 5) / 256

    return depth_meters

class Weights_Save():

    def __init__(self,optimizer, model, path):
        self.optimizer = optimizer
        self.model = model
        self.path = path
        
    def save_checkpoint(self):

    # Basic details
        time.sleep(10)
        checkpoint = {
            'state_dict': self.model.module.state_dict(),
            'optimizer': self.optimizer,
            'optimizer_state_dict': self.optimizer.state_dict()
        }
        
        torch.save(checkpoint, self.path)


class Posed_model_points():

    def __init__(self,model_points):
        self.model_points = torch.tensor(model_points)

    def quaternion_mult(self,q,r):
        return torch.tensor([r[0]*q[0]-r[1]*q[1]-r[2]*q[2]-r[3]*q[3],
                r[0]*q[1]+r[1]*q[0]-r[2]*q[3]+r[3]*q[2],
                r[0]*q[2]+r[1]*q[3]+r[2]*q[0]-r[3]*q[1],
                r[0]*q[3]-r[1]*q[2]+r[2]*q[1]+r[3]*q[0]])

    def point_rotation_by_quaternion(self, point,q):

        r = torch.cat((torch.tensor([0.0]), point))
        q_conj = [q[0],-1*q[1],-1*q[2],-1*q[3]]
        return self.quaternion_mult(self.quaternion_mult(q,r),q_conj)[1:]

    def pose_applied(self, pose_quaternions, translation):
        resultant = torch.zeros((100,3))

        for count,i in enumerate(self.model_points):
            
            vec = self.point_rotation_by_quaternion(i, pose_quaternions) + translation
            resultant[count] = vec
            break
        return resultant

class Posed_model_points_numpy_version():

    def __init__(self,model_points):
        self.model_points = np.array(model_points)

    def quaternion_mult(self,q,r):
        return [r[0]*q[0]-r[1]*q[1]-r[2]*q[2]-r[3]*q[3],
                r[0]*q[1]+r[1]*q[0]-r[2]*q[3]+r[3]*q[2],
                r[0]*q[2]+r[1]*q[3]+r[2]*q[0]-r[3]*q[1],
                r[0]*q[3]-r[1]*q[2]+r[2]*q[1]+r[3]*q[0]]

    def point_rotation_by_quaternion(self, point,q):
        r = [0]+point
        q_conj = [q[0],-1*q[1],-1*q[2],-1*q[3]]
        return self.quaternion_mult(self.quaternion_mult(q,r),q_conj)[1:]

    def points_rotated(self, pose_quaternions):
        resultant = np.zeros((50,3))

        for i, count in enumerate(self.model_points):
            vec = self.point_rotation_by_quaternion(i, pose_quaternions)
            resultant[count] = vec

        return resultant
