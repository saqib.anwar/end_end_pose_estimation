import torch
import torch.nn as nn
import torchvision.models as models

class Resnet_pose_prediction(nn.Module):
    def __init__(self):
        super(Resnet_pose_prediction, self).__init__()
        
        self.resnet_101 = models.resnet101(pretrained=True)
        self.resnet = nn.Sequential(*list(self.resnet_101.children())[:-2])

        for count, child in enumerate(self.resnet.children()):
            if count<6:
                for param in child.parameters():
                    param.requires_grad = False

        self.conv1 = nn.Conv2d(in_channels = 2048, out_channels= 1024, kernel_size= (3,3), stride = 1)
        self.bn1 = nn.BatchNorm2d(num_features = 1024)
        self.conv2 = nn.Conv2d(in_channels = 1024, out_channels= 1024, kernel_size= (3,3), stride = 1)
        self.bn2 = nn.BatchNorm2d(num_features = 1024)
        self.conv3 = nn.Conv2d(in_channels = 1024, out_channels= 1024, kernel_size= (3,3), stride = 1)
        self.bn3 = nn.BatchNorm2d(num_features = 1024)
        self.relu = nn.ReLU(inplace= True)
        
        self.fc1 = nn.Linear(129024, 2048)
        self.fc2 = nn.Linear(2048, 1024)
        self.fc3 = nn.Linear(1024, 512)
        
    def forward(self, x):
        x = self.resnet(x)
        x = self.bn1(self.conv1(x))
        x = self.bn2(self.conv2(x))
        x = self.relu(self.bn3(self.conv3(x)))
        x = x.view(-1, 1024*9*14)
        x = nn.functional.relu(self.fc1(x))
        x = nn.functional.relu(self.fc2(x))
        x = self.fc3(x)
        
        return x
        
        