from PIL import Image
import pandas as pd
import os
import os.path
import numpy as np
import torchvision.transforms as transforms
import argparse
import time
import random
import numpy.ma as ma
import copy
import scipy.misc
import scipy.io as scio
import pickle
import matplotlib.pyplot as plt
import torchvision.transforms as transform
import torch.nn.functional as F
import json
import argparse
import os
from pspnet import PSPNet# this python file is available in the directory
import matplotlib.pyplot as plt

import torch
import torch.utils.data as data
from torch.utils.data import DataLoader
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
from torch.autograd import Variable
import math

from virtual_dataset_train import Virtual_PoseDataset
from virtual_dataset_add_evaluation import Virtual_Evaluation_Dataset
from real_dataset_add_evaluation import Real_Evaluation_Dataset
from neural_net import Change_PoseNet
from neural_net import PoseRefineNet
from loss_file import Multi_Batch_Modified_Loss
from loss_file import Loss_refine
from utils import Util_Functions
from post_processing import Weights_Save

xyz_path = '006_mustard_bottle/points.xyz'
util_functions = Util_Functions('bottle_pointcloud.pickle', xyz_path, 1000)
real_data_pcl_file = util_functions.xyz_cloud_generation()
virtual_data_pcl_file = util_functions.load_pointcloud()

load_checkpoint = False

if load_checkpoint:
    check_path = ''  # add path to resume training from a checkpoint
    load_point = torch.load(check_path)
    

estimator = Change_PoseNet(num_points = 500, num_obj = 1)
#estimator.load_state_dict(load_point['state_dict'])
estimator = nn.DataParallel(estimator)
estimator.cuda()

# refiner = PoseRefineNet(num_points = 500, num_obj = 1)
# refiner = nn.DataParallel(refiner)
# refiner.cuda()

def add_meteric(pred_r, pred_t, pred_c, target, model_points, points, num_point_mesh= 500):
    bs, num_p, _ = pred_c.size()

    pred_r = pred_r / (torch.norm(pred_r, dim=2).view(bs, num_p, 1))
    
    base = torch.cat(((1.0 - 2.0*(pred_r[:, :, 2]**2 + pred_r[:, :, 3]**2)).view(bs, num_p, 1),\
                    (2.0*pred_r[:, :, 1]*pred_r[:, :, 2] - 2.0*pred_r[:, :, 0]*pred_r[:, :, 3]).view(bs, num_p, 1), \
                    (2.0*pred_r[:, :, 0]*pred_r[:, :, 2] + 2.0*pred_r[:, :, 1]*pred_r[:, :, 3]).view(bs, num_p, 1), \
                    (2.0*pred_r[:, :, 1]*pred_r[:, :, 2] + 2.0*pred_r[:, :, 3]*pred_r[:, :, 0]).view(bs, num_p, 1), \
                    (1.0 - 2.0*(pred_r[:, :, 1]**2 + pred_r[:, :, 3]**2)).view(bs, num_p, 1), \
                    (-2.0*pred_r[:, :, 0]*pred_r[:, :, 1] + 2.0*pred_r[:, :, 2]*pred_r[:, :, 3]).view(bs, num_p, 1), \
                    (-2.0*pred_r[:, :, 0]*pred_r[:, :, 2] + 2.0*pred_r[:, :, 1]*pred_r[:, :, 3]).view(bs, num_p, 1), \
                    (2.0*pred_r[:, :, 0]*pred_r[:, :, 1] + 2.0*pred_r[:, :, 2]*pred_r[:, :, 3]).view(bs, num_p, 1), \
                    (1.0 - 2.0*(pred_r[:, :, 1]**2 + pred_r[:, :, 2]**2)).view(bs, num_p, 1)), dim=2).contiguous().view(bs * num_p, 3, 3)

    ori_base = base
    base = base.contiguous().transpose(2, 1).contiguous()
    model_points = model_points.view(bs, 1, num_point_mesh, 3).repeat(1, num_p, 1, 1).view(bs * num_p, num_point_mesh, 3)
    target_no_change = target  # -> (32,1000,3)
    target = target.view(bs, 1, num_point_mesh, 3).repeat(1, num_p, 1, 1).view(bs * num_p, num_point_mesh, 3)
    ori_target = target
    pred_t = pred_t.contiguous().view(bs * num_p, 1, 3)
    ori_t = pred_t
    points = points.contiguous().view(bs * num_p, 1, 3)
    pred_c = pred_c.contiguous().view(bs * num_p)

    pred = torch.add(torch.bmm(model_points, base), points + pred_t)

    dis = torch.mean(torch.norm((pred - target), dim=2), dim=1)
    dis = dis.view(bs, num_p)
    pred_c = pred_c.view(bs, num_p)
    how_max, which_max = torch.max(pred_c, 1)
    which_max_dis = which_max.unsqueeze(-1)
    high_confidence_dis = torch.gather(dis, 1, which_max_dis)
    high_confidence_dis = high_confidence_dis.view(bs)
    high_confidence_dis = torch.mean(high_confidence_dis)

    return high_confidence_dis.item()


########       DEFINING DATA_LOADER     #############

def pickle_load(path):
    return pickle.load(open(path, 'rb'))

data_path_virtual = '/home/thesis_main_folder/dfusion/data_cleaning/wo_distractors_filtered_files_429893.pickle' 
#data_path_real = '/home/dope_inference_notebook/mustard_real_data_complete_32321.pickle'

virtual_data_list = pickle_load(data_path_virtual)
#real_data_list = pickle_load(data_path_real)

train_set_list = virtual_data_list[:250000]
validation_set_list = virtual_data_list[250000:300000]

batch_size_defined = 200

train_set = Virtual_PoseDataset(train_set_list, virtual_data_pcl_file)
train_loader = DataLoader(train_set, batch_size=batch_size_defined, shuffle =True, num_workers = 8)

validation_set = Virtual_PoseDataset(validation_set_list, virtual_data_pcl_file)
validation_loader = DataLoader(validation_set, batch_size=batch_size_defined, shuffle =True, num_workers = 8)

refine_start = False
refine_margin = 0.018
decay_start = False
refiner_iteration = 2
decay_margin = 0.016
lr = 0.000001#/10.0
lr_rate = 0.3
w = 0.015
w_rate = 0.3
optimizer = optim.Adam(estimator.parameters(), lr= lr)
optimizer = load_point['optimizer']
optimizer.load_state_dict(load_point['optimizer_state_dict'])
for param_group in optimizer.param_groups:
    param_group['lr'] = lr

criterion = Multi_Batch_Modified_Loss(500, [])
criterion_refine = Loss_refine(500, [])


import logging

def setup_logger(logger_name, log_file, level=logging.INFO):
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s : %(message)s')
    fileHandler = logging.FileHandler(log_file, mode='w')
    fileHandler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(fileHandler)

    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    l.addHandler(streamHandler)
    return l


#***********************************************************TRAINING LOOP STARTS HERE**************************************************************

estimator.train_data_len = len(train_set)
estimator.test_data_len = len(validation_set)
estimator.synthetic_data_type = 'non-distracted synthetic data'
estimator.epochs = 0


best_test = np.Inf
train_error_log = setup_logger('full_train_error', os.path.join('results/single_wo_distractor/feb_10/logger/', 'full_training_error_log.txt'))
test_error_log = setup_logger('full_test_error', os.path.join('results/single_wo_distractor/feb_10/logger/', 'full_test_error_log.txt'))

total_epochs = 300
st_time = time.time()

# LISTS FOR DATAFRAME
count_list = []   # this is the serial number 
epoch_list = []   # this contains the epoch
lr_list = []      # contains the learning rate used
batch_size_list = []    # contains the batch size
data_length_list = []   # contains the lengths of trian_set and test_set
w_hyperparameter_list = []   # contains the value of the w hyperparmeter
pred_c_list = [] 
pred_c_mean = []
epoch_mean = []
batch_list= []   # contains the batch number
train_loss_list = []   
train_dis_list = []


train_loss_mean = []
train_dis_mean = []
test_loss_mean = []
test_dis_mean = []


total_count = 0
per_epoch_time = 0.0

for epoch in range(15, total_epochs):

    estimator.epochs = epoch
    epoch_time_start = time.time()
    logger = setup_logger('epoch%d' % epoch, os.path.join('results/single_wo_distractor/feb_10/logger/per_epoch_trianing_log', 'epoch_%d_log.txt' % epoch))
    logger.info('Train time {0}'.format(time.strftime("%Hh %Mm %Ss", time.gmtime(time.time() - st_time)) + ', ' + 'Training started'))
    train_count = 0
    train_dis_avg = 0.0
    full_train_error = 0.0
    full_train_num = 0
    batch_count = 1
    epoch_mean.append(epoch)
    
    estimator.train()
    optimizer.zero_grad()

    for i, data in enumerate(train_loader, 0):     # for loop for batches

        points, choose, img, target, model_points, idx = data
        points, choose, img, target, model_points, idx = Variable(points).cuda(), \
                                                            Variable(choose).cuda(), \
                                                            Variable(img).cuda(), \
                                                            Variable(target).cuda(), \
                                                            Variable(model_points).cuda(), \
                                                            Variable(idx).cuda()

        optimizer.zero_grad()

        pred_r, pred_t, pred_c, emb = estimator(img, points, choose, idx)
        loss, dis, new_points, new_target, pred_c_1 = criterion(pred_r, pred_t, pred_c, target, model_points, idx, points, w, refine_start)
        
        dis.backward()
        optimizer.step()

        train_dis_avg += dis.item()   # provides the average loss over the whole epoch
        train_count += 1
        full_train_error += dis.item()
        full_train_num += 1

        show_time = 1
        logger.info('Train time {0} Epoch {1} Batch {2} Frame {3} Avg_dis:{4} Loss_val:{5} Per_epoch_time: {6}'.format(time.strftime("%Hh %Mm %Ss", time.gmtime(time.time() - st_time)), epoch, int(train_count / show_time), train_count, train_dis_avg / show_time, loss.item(), per_epoch_time))
        
        
        train_dis_avg = 0
        epoch_list.append(epoch)
        count_list.append(total_count)
        train_loss_list.append(loss.item())
        train_dis_list.append(dis.item())
        batch_list.append(i)
        pred_c_list.append(pred_c_1)
        
        total_count += 1

        torch.save(estimator.state_dict(), 'results/single_wo_distractor/feb_10/weights/all/pose_model_epoch_{0}.pth'.format(estimator.epochs))
        
        batch_count += 1

    #    train_error_log.info('Train time {0} Epoch {1} Avg_error_dis:{2}'.format(time.strftime("%Hh %Mm %Ss", time.gmtime(time.time() - st_time)), epoch, full_train_error / full_train_num))

    
    print('>>>>>>>>----------epoch {0} train finish---------<<<<<<<<'.format(epoch))


    logger = setup_logger('epoch%d_test' % epoch, os.path.join('results/single_wo_distractor/feb_10/logger/per_epoch_test_log', 'epoch_%d_test_log.txt' % epoch))
    logger.info('Test time {0}'.format(time.strftime("%Hh %Mm %Ss", time.gmtime(time.time() - st_time)) + ', ' + 'Testing started'))
    test_dis = 0.0
    test_count = 0
    estimator.eval()
    #refiner.eval()

    test_loss_list = []
    test_dis_list = []

    for j, data in enumerate(validation_loader, 0):
        #print('****************NOW INSIDE VALIDATION LOADER**********************')
        points, choose, img, target, model_points, idx = data
        points, choose, img, target, model_points, idx = Variable(points).cuda(), \
                                                            Variable(choose).cuda(), \
                                                            Variable(img).cuda(), \
                                                            Variable(target).cuda(), \
                                                            Variable(model_points).cuda(), \
                                                            Variable(idx).cuda()
        pred_r, pred_t, pred_c, emb = estimator(img, points, choose, idx)
        pred_r, pred_t, pred_c, emb = pred_r.detach(), pred_t.detach(), pred_c.detach(), emb.detach()
        loss, dis, new_points, new_target, pred_c_1 = criterion(pred_r, pred_t, pred_c, target, model_points, idx, points, w, refine_start)

        test_loss_list.append(loss.item())
        test_dis_list.append(dis.item())

        test_dis += dis.item()
        logger.info('Test time {0} Test Frame No.{1} dis:{2}'.format(time.strftime("%Hh %Mm %Ss", time.gmtime(time.time() - st_time)), test_count, dis))

        test_count += 1
    
    data_dict = {'count':count_list, 'epochs':epoch_list, 'batch_number':batch_list, 'train_loss':train_loss_list, 'train_dis':train_dis_list}#, 'test_loss':test_loss_list, 'test_dis':test_dis_list}
    df = pd.DataFrame(data= data_dict)
    save_df = df.to_csv('results/single_wo_distractor/feb_10/pandas/per_batch_loss_dataframe.csv', index= False)
    plt.plot(df['count'], df['train_loss'], label = 'train_loss')
    plt.plot(df['count'], df['train_dis'], label = 'train_dis')
    plt.title('Per Batch Loss Visualization')
    plt.legend()
    plt.savefig('results/single_wo_distractor/feb_10/pandas/per_batch_losses.png')
    plt.close()

    total_epochs=df['epochs'].unique()


    filter_row = df[df['epochs']== epoch]
    train_loss_mean.append(filter_row['train_loss'].mean())
    test_loss_mean.append(np.mean(np.array(test_loss_list)))
    train_dis_mean.append(filter_row['train_dis'].mean())
    test_dis_mean.append(np.mean(np.array(test_dis_list)))

    # lines for checkpoint save
    estimator.train_loss = filter_row['train_dis'].mean()
    estimator.validation_loss = np.mean(np.array(test_dis_list))


    lr_list.append(lr)
    batch_size_list.append(batch_size_defined)
    data_length_list.append((len(train_set), len(validation_set)))
    w_hyperparameter_list.append(w)
    pred_c_mean.append(np.mean(np.array(pred_c_list)))

    mean_data_dict = {'epochs':epoch_mean, 'lr_value':lr_list, 'train_loss_mean':train_loss_mean, 'train_dis_mean':train_dis_mean, 'test_loss_mean': test_loss_mean, 'test_dis_mean':test_dis_mean, 'batch_size':batch_size_list, 'data_length':data_length_list, 'w_hyperparameter': w_hyperparameter_list, 'pred_c':pred_c_mean}
    mean_df = pd.DataFrame(data= mean_data_dict)
    mean_save_df = mean_df.to_csv('results/single_wo_distractor/feb_10/pandas/per_epoch_loss_dataframe.csv', index= False)

    plt.plot(mean_df['epochs'], mean_df['train_loss_mean'], label= 'train_loss_mean')
    plt.plot(mean_df['epochs'], mean_df['test_loss_mean'], label = 'test_loss_mean')
    plt.plot(mean_df['epochs'], mean_df['train_dis_mean'], label = 'train_dis_mean')
    plt.plot(mean_df['epochs'], mean_df['test_dis_mean'], label = 'test_dis_mean')
    plt.title('Per Epoch Loss Visualization')
    plt.legend()
    plt.savefig('results/single_wo_distractor/feb_10/pandas/per_epoch_losses.png')
    plt.close()


    test_dis = test_dis / test_count
    logger.info('Test time {0} Epoch {1} TEST FINISH Avg dis: {2}'.format(time.strftime("%Hh %Mm %Ss", time.gmtime(time.time() - st_time)), epoch, test_dis))
    test_error_log.info('Test time {0} Epoch {1} TEST FINISH Avg dis: {2}'.format(time.strftime("%Hh %Mm %Ss", time.gmtime(time.time() - st_time)), epoch, test_dis))
    

    if test_dis <= best_test:
        best_test = test_dis

        torch.save(estimator.state_dict(), '{0}/pose_model_{1}_{2}.pth'.format('results/single_wo_distractor/feb_10/weights/best/state_dicts', epoch, test_dis))
        weights_save = Weights_Save(optimizer, estimator, 'results/single_wo_distractor/feb_10/weights/best/checkpoints/estimator_weights_epoch_{0}_{1}.pth'.format(estimator.epochs, estimator.train_loss))
        weights_save.save_checkpoint()

        print(epoch, '>>>>>>>>----------BEST TEST MODEL SAVED---------<<<<<<<<')

    if best_test < decay_margin and not decay_start:
        decay_start = True
        lr *= lr_rate
        w *= w_rate
        optimizer = optim.Adam(estimator.parameters(), lr= lr)

        
    per_epoch_time = time.strftime("%Hh %Mm %Ss", time.gmtime(time.time() - epoch_time_start))