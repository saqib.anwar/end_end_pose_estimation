import os
import os.path
import numpy as np
import argparse
import time
import random
import numpy.ma as ma
import copy
import pickle
import json
import argparse
import os

import math


class Util_Functions():
    def __init__(self, virtual_bottle_pcl_path, real_bottle_pcl_path, number_of_points):
        self.virtual_bottle_pcl_path = virtual_bottle_pcl_path
        self.real_bottle_pcl_path = real_bottle_pcl_path
        self.number_of_points = number_of_points

    def xyz_cloud_generation(self):
        
        input_file = open(self.real_bottle_pcl_path)
        cld = []
        while 1:
            input_line = input_file.readline()
            if not input_line:
                break
            input_line = input_line[:-1]
            input_line = input_line.split(' ')
            cld.append([float(input_line[0]), float(input_line[1]), float(input_line[2])])
        input_file.close()
        cld = np.array(cld)
        random_indices = np.random.choice(cld.shape[0], size= self.number_of_points, replace = False)
        cld = cld[random_indices, :]
        
        return cld

    def load_pointcloud(self):
        
        bottle_pointcloud = pickle.load(open(self.virtual_bottle_pcl_path, 'rb'))
        random_indices = np.random.choice(bottle_pointcloud.shape[0], size= self.number_of_points, replace = False)
        bottle_pointcloud = bottle_pointcloud[random_indices, :]
        return (bottle_pointcloud)
