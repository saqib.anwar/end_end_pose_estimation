#!/bin/bash

set -x
set -e

export PYTHONUNBUFFERED="True"
export CUDA_VISIBLE_DEVICES=1,2,3,4,5
#export CUDA_LAUNCH_BLOCKING=1

python3.6 train_2.py

