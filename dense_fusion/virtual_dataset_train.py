from PIL import Image
import pandas as pd
import os
import os.path
import numpy as np
import torchvision.transforms as transforms
import argparse
import time
import random
import numpy.ma as ma
import copy
import scipy.misc
import scipy.io as scio
import pickle
import matplotlib.pyplot as plt
import torchvision.transforms as transform
import torch.nn.functional as F
import json
import argparse
import os
from pspnet import PSPNet# this python file is available in the directory
import matplotlib.pyplot as plt

import torch
import torch.utils.data as data
from torch.utils.data import DataLoader
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
from torch.autograd import Variable
import math



camera_matrix = {'cx': 339.9808,
                'cy': 243.0,
                'fx':632.9641,
                'fy':638.2669}


def bounding_box(img):
    img_array = np.array(img)
    rmin = np.nonzero(img_array)[0].min() 
    rmax = np.nonzero(img_array)[0].max() 
    cmin = np.nonzero(img_array)[1].min()
    cmax = np.nonzero(img_array)[1].max()
    
    return rmin, rmax, cmin, cmax

def bounded_img(img):
    rmin, rmax, cmin, cmax = bounding_box(img)
    img_array = np.array(img)
    img_array = img_array[rmin:rmax, cmin:cmax]
    return Image.fromarray(img_array)


def img_mask_formation(img, label_img, rmin, rmax, cmin, cmax):

    img_mask = np.transpose(img, (2,0,1))[:,rmin:rmax, cmin:cmax]
    img_mask = np.transpose(img_mask,(1,2,0))
    img_mask = Image.fromarray(img_mask)
    return img_mask

def choose_points(label_img, depth_img, rmin, rmax, cmin, cmax, total_points):
    mask_label = ma.getmaskarray(ma.masked_not_equal(label_img, 0))
    mask = mask_label * np.array(depth_img)
    
    choose = mask[rmin:rmax, cmin:cmax].flatten().nonzero()[0]
    if len(choose)> total_points:
        points = np.zeros(len(choose))
        points[:total_points] = 1
        np.random.shuffle(points)
        points_chosen = np.nonzero(points)
        choose = choose[points_chosen[0]]
    else:
        choose = np.pad(choose, (0, total_points - len(choose)), 'wrap')
    return choose
    
def load_model_points(path):
    bottle_pointcloud = pickle.load(open(path, 'rb'))
    return (bottle_pointcloud)

def depth_mask(depth_img, label_img, rmin, rmax, cmin, cmax):
    mask_binary = ma.getmaskarray(ma.masked_not_equal(label_img, 0))
    mask_select = mask_binary * depth_img
    mask_select = mask_select[rmin:rmax, cmin:cmax]
    mask = Image.fromarray(mask_select)
    return mask


def img_to_pcl_f2(depth_scan, seg, camera_matrix):

    cam_cx = camera_matrix['cx']
    cam_cy = camera_matrix['cy']
    cam_fx = camera_matrix['fx']
    cam_fy = camera_matrix['fy']
    
    x_list = []
    y_list = []
    z_list = []
    depth_scan = np.array(depth_scan)
    
    cam_scale = 256/5.0
    
    indices = np.nonzero(seg)
    for i in range(len(indices[0])):
        z = (depth_scan[indices[0], indices[1]])/cam_scale
        
        u = indices[1]
        v = indices[0]
        x_over_z = (u - cam_cx) / cam_fx
        y_over_z = (v - cam_cy) / cam_fy

        x = x_over_z * z
        y = y_over_z * z
        
        x_list.append(x), y_list.append(y), z_list.append(z)
        x_list, y_list, z_list = np.array(x_list), np.array(y_list), np.array(z_list)
        return np.average(x_list), np.average(y_list), np.average(z_list)


def img_to_pcl_tensor(depth_scan, seg, camera_matrix):

    pcl_tensor = np.zeros((3,480,640))
    
    cam_cx = camera_matrix['cx']
    cam_cy = camera_matrix['cy']
    cam_fx = camera_matrix['fx']
    cam_fy = camera_matrix['fy']
    
    x_list = []
    y_list = []
    z_list = []
    depth_scan = np.array(depth_scan)
    cam_scale = 256/500
    
    for i in range(480):
        for j in range(640):
            u, v = i, j
            z = (depth_scan[i,j])/cam_scale
            x_over_z = (u - cam_cx) / cam_fx
            y_over_z = (v - cam_cy) / cam_fy
            
            x = x_over_z * z
            y = y_over_z * z
            
            pcl_tensor[0, i, j] = x
            pcl_tensor[1, i, j] = y
            pcl_tensor[2, i, j] = z
            
    return pcl_tensor



def depth_img_to_pcl(depth_img,label, choose, camera_matrix, rmin, rmax, cmin, cmax):
    depth_img = np.array(depth_img)
    depth_masked = depth_img[rmin:rmax, cmin:cmax].flatten()[choose][:, np.newaxis].astype(np.float32)
    xmap = np.array([[j for i in range(640)] for j in range(480)])
    ymap = np.array([[i for i in range(640)] for j in range(480)])
    xmap_masked = xmap[rmin:rmax, cmin:cmax].flatten()[choose][:, np.newaxis].astype(np.float32)
    ymap_masked = ymap[rmin:rmax, cmin:cmax].flatten()[choose][:, np.newaxis].astype(np.float32)
    choose = np.array(choose)

    cam_cx = camera_matrix['cx']
    cam_cy = camera_matrix['cy']
    cam_fx = camera_matrix['fx']
    cam_fy = camera_matrix['fy']
    
    cam_scale = 256/5.0 #meters it is inverted as we are dividing
    pt2 = depth_masked / cam_scale
    pt0 = (ymap_masked - cam_cx) * pt2 / cam_fx
    pt1 = (xmap_masked - cam_cy) * pt2 / cam_fy
    cloud = np.concatenate((pt0, pt1, pt2), axis=1)

    return cloud
    

def target_pointcloud_cm(transformation_matrix, pointcloud):
    
    number_of_points = 500
    pointcloud = np.array(pointcloud) * 100
    
    # adding few more lines to keep only thousand points
    dellist = [j for j in range(len(pointcloud))]
    dellist = random.sample(dellist, (len(pointcloud) - number_of_points))
    pointcloud = np.delete(pointcloud, dellist,axis = 0)
    model_points= pointcloud
    
    target_r = np.array(transformation_matrix)[:3,:3]
    target_t = np.array(transformation_matrix)[-1,:3]
    
    pointcloud = np.dot(pointcloud, target_r)
    pointcloud = np.add(pointcloud, target_t)
    
    return pointcloud, model_points


def target_pointcloud_meters(transformation_matrix, pointcloud):
    
    number_of_points = 500
    pointcloud = np.array(pointcloud)# * 100
    
    # adding few more lines to keep only thousand points
    dellist = [j for j in range(len(pointcloud))]
    dellist = random.sample(dellist, (len(pointcloud) - number_of_points))
    pointcloud = np.delete(pointcloud, dellist,axis = 0)
    model_points= pointcloud
    
    target_r = np.array(transformation_matrix)[:3,:3]
    target_t = np.array(transformation_matrix)[-1,:3] / 100
    
    pointcloud = np.dot(pointcloud, target_r)
    pointcloud = np.add(pointcloud, target_t)
    
    return pointcloud, model_points



class Image_Transform(object):
    def __init__(self, x_max, y_max):
        self.x_max = x_max
        self.y_max = y_max
        
    def __call__(self, sample):
        image = sample
        image_array = np.array(image)
        img_x, img_y = image_array.shape[0], image_array.shape[1]
        
        trancolor = transform.ColorJitter(0.3, 0.3, 0.3, 0.05)
        image = trancolor(image)
        image = np.array(image)
        
        if  img_x > self.x_max:
            x_crop = img_x - self.x_max
            x_crop_1 = x_crop//2
            x_crop_2 = x_crop - x_crop_1

            image = (image)[x_crop_1: -x_crop_2, :]
            
        if img_y > self.y_max:
            y_crop = img_y - self.y_max
            y_crop_1 = y_crop//2
            y_crop_2 = y_crop - y_crop_1
            
            image = image[:, y_crop_1: -y_crop_2]
            
        padded = torch.from_numpy(image)    
        padded = padded.numpy()
        
        padded_img = transform.ToTensor()(padded)
        norm = transform.Normalize(mean=[0.48325, 0.46121, 0.37687], std=[0.19693, 0.19784, 0.191587])
        norm_img = norm(padded_img)
        
        return norm_img 



class Virtual_PoseDataset(data.Dataset):
    def __init__(self, datapath, bottle_pointcloud):
        self.datalist = datapath
        self.bottle_pointcloud = bottle_pointcloud
        
    def __getitem__(self, index):
        img, label, depth_img, json_file, pfile = self.datalist[index]
        img = img.replace('/run/user/1000/gvfs/smb-share:server=149.201.37.175,share=anwar/', '/home/')
        label = label.replace('/run/user/1000/gvfs/smb-share:server=149.201.37.175,share=anwar/', '/home/')
        depth_img = depth_img.replace('/run/user/1000/gvfs/smb-share:server=149.201.37.175,share=anwar/', '/home/')
        json_file = json_file.replace('/run/user/1000/gvfs/smb-share:server=149.201.37.175,share=anwar/', '/home/')
        img = Image.open(img).convert('RGB')
        label = Image.open(label)
        depth_img = Image.open(depth_img)
        json_data = json.load(open(json_file))

        rmin = math.ceil(json_data['objects'][0]['bounding_box']['top_left'][0])
        rmax = math.ceil(json_data['objects'][0]['bounding_box']['bottom_right'][0])
        cmin = math.ceil(json_data['objects'][0]['bounding_box']['top_left'][1])
        cmax = math.ceil(json_data['objects'][0]['bounding_box']['bottom_right'][1])

        #import math
        if rmin <0:
            rmin = 0
        if rmax > 480:
            rmax = 480
        if cmin < 0:
            cmin = 0
        if cmax > 640:
            cmax = 640

        #import math
        if rmax - rmin > 200:
            to_subtract = ((rmax - rmin) - 200)/2
            to_subtract = int(math.ceil(to_subtract))
            rmin = rmin + to_subtract
            rmax = rmax - to_subtract

        if cmax - cmin > 200:
            to_subtract = ((cmax - cmin) - 200)/2
            to_subtract = int(math.ceil(to_subtract))
            cmin = cmin + to_subtract
            cmax = cmax - to_subtract
            
        if rmax - rmin < 200:
            to_add = (200 - (rmax - rmin))
            
            randint = random.randint(0, to_add)
            
            rmin = rmin - randint
            rmax = rmax + (to_add - randint)

            if rmin < 0:
                rmax = rmax - rmin
                rmin = 0
            
            if rmax > 480:
                rmin = rmin - (rmax - 480)
                rmax = 480
                
            
        if cmax - cmin < 200:
            to_add = (200 - (cmax - cmin))
                    
            randint = random.randint(0, to_add)

            cmin = cmin - randint
            cmax = cmax + (to_add - randint)
            
            if cmin < 0:
                cmax = cmax - cmin
                cmin = 0
            
            if cmax > 640:
                cmin = cmin - (cmax - 640)
                cmax = 640
        
        pose_transform = json_data['objects'][0]['pose_transform']
        json_data = json_data['objects'][0]
        
        camera_matrix = {'cx': 339.9808,
                        'cy': 243.0,
                        'fx':632.9641,
                        'fy':638.2669}
        
        img_mask = img_mask_formation(img, label, rmin, rmax, cmin, cmax)
        img_transform = Image_Transform(200,200)
        img_mask = img_transform(img_mask)
        
        choose = choose_points(label, depth_img, rmin, rmax, cmin, cmax, 500)   
        cloud = depth_img_to_pcl(depth_img, label, choose, camera_matrix, rmin, rmax, cmin, cmax) 
        choose = np.expand_dims(choose, axis = 0)
        bottle_pointcloud = self.bottle_pointcloud
        target, model_points = target_pointcloud_meters(pose_transform, bottle_pointcloud) 

        return torch.from_numpy(cloud.astype(np.float32)),\
               torch.LongTensor(choose.astype(np.int32)),\
               img_mask,\
               torch.from_numpy(target.astype(np.float32)),\
               torch.from_numpy(model_points.astype(np.float32)),\
               torch.LongTensor([0])
        
    def __len__(self):
        return len(self.datalist)