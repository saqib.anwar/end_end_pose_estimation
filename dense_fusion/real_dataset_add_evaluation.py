from PIL import Image
import pandas as pd
import os
import os.path
import numpy as np
import torchvision.transforms as transforms
import argparse
import time
import random
import numpy.ma as ma
import copy
import scipy.misc
import scipy.io as scio
import pickle
import matplotlib.pyplot as plt
import torchvision.transforms as transform
import torch.nn.functional as F
import json
import argparse
import os
from pspnet import PSPNet# this python file is available in the directory
import matplotlib.pyplot as plt

import torch
import torch.utils.data as data
from torch.utils.data import DataLoader
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
from torch.autograd import Variable
import math
import scipy.io as scio

def label_bottle_separate(label_img):
    # takes the image and returns the image of the separated bottle
    label = np.array(label_img)
    a = np.zeros(label.shape)

    for i in range(label.shape[0]):
        for j in range(label.shape[1]):
            if np.array(label)[i][j] == 5:
                a[i][j] = 255
            else:
                a[i][j] = 0
    final_img = Image.fromarray(a)
    # if img.mode != 'RGB':
    final_img = final_img.convert('L')
    
    return final_img

def choose_points(label_img, depth_img, total_points):
    mask_label = np.array(label_img)
    mask = mask_label * np.array(depth_img)
    
    choose = mask.flatten().nonzero()[0]
    if len(choose)> total_points:
        points = np.zeros(len(choose))
        points[:total_points] = 1
        np.random.shuffle(points)
        points_chosen = np.nonzero(points)
        choose = choose[points_chosen[0]]
    else:
        choose = np.pad(choose, (0, total_points - len(choose)), 'wrap')
    return choose


def depth_img_to_pcl(depth_img,label, choose, camera_matrix):
    depth_img = np.array(depth_img)
    depth_masked = depth_img.flatten()[choose][:, np.newaxis].astype(np.float32)
    xmap = np.array([[j for i in range(640)] for j in range(480)])
    ymap = np.array([[i for i in range(640)] for j in range(480)])
    xmap_masked = xmap.flatten()[choose][:, np.newaxis].astype(np.float32)
    ymap_masked = ymap.flatten()[choose][:, np.newaxis].astype(np.float32)
    choose = np.array(choose)

    cam_cx = camera_matrix['cx']
    cam_cy = camera_matrix['cy']
    cam_fx = camera_matrix['fx']
    cam_fy = camera_matrix['fy']
    
    #cam_scale = meta['factor_depth'][0][0]
    #cam_scale = 256/10.0 #since we are dividing so inverted
    cam_scale = 10000
    pt2 = depth_masked / cam_scale
    pt0 = (ymap_masked - cam_cx) * pt2 / cam_fx
    pt1 = (xmap_masked - cam_cy) * pt2 / cam_fy
    cloud = np.concatenate((pt0, pt1, pt2), axis=1)

    return cloud


def target_pointcloud(transformation_matrix, pointcloud):
    
    number_of_points = 500
    pointcloud = np.array(pointcloud) #* 100
    
    # adding few more lines to keep only thousand points
    dellist = [j for j in range(len(pointcloud))]
    dellist = random.sample(dellist, (len(pointcloud) - number_of_points))
    pointcloud = np.delete(pointcloud, dellist,axis = 0)
    model_points= pointcloud
    
    target_r = np.array(transformation_matrix)[:,:3]
    target_t = np.array(transformation_matrix)[:,3]
    
    pointcloud = np.dot(pointcloud, target_r)
    pointcloud = np.add(pointcloud, target_t)
    
    return pointcloud, model_points



class Image_Transform(object):
    def __init__(self, x_max, y_max):
        self.x_max = x_max
        self.y_max = y_max
        
    def __call__(self, sample):
        image = sample
        image_array = np.array(image)
        
        padded_img = transform.ToTensor()(image_array)
        norm = transform.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        norm_img = norm(padded_img)
        
        return norm_img #padded#, img

class Real_Evaluation_Dataset(data.Dataset):
    def __init__(self, datapath, xyz_cloud):
        self.datalist = datapath
        self.xyz_cloud = xyz_cloud
        
    def __getitem__(self, index):
        
        bbox_file = self.datalist[index]
        bbox_file = bbox_file.replace('/run/user/1000/gvfs/smb-share:server=149.201.37.175,share=anwar/', '/home/')
        img = bbox_file.replace('-box.txt', '-color.png')
        label = bbox_file.replace('-box.txt', '-label.png')
        depth = bbox_file.replace('-box.txt', '-depth.png')
        meta_file = bbox_file.replace('-box.txt', '-meta.mat')

        img = Image.open(img)
        label = Image.open(label)
        depth_img = Image.open(depth)
        meta_file = scio.loadmat(meta_file)

        f = open(bbox_file, 'r')
        for i in range(10):
            check = f.readline()
            if check.split(' ')[0]  == '006_mustard_bottle':
                txt = check.split(' ')#[-1].split(' ')
                rmax = math.ceil(float(txt[1]))
                rmin = math.floor(float(txt[2]))
                cmax = math.ceil(float(txt[3]))
                cmin = math.floor(float(txt[4]))
                break         
            else:
                continue

        camera_matrix = {'cx': 339.9808,
                'cy': 243.0,
                'fx':632.9641,
                'fy':638.2669}

        check_val = 5
        place = np.argwhere(meta_file['cls_indexes']== check_val)[0][0]
        pose = meta_file['poses'][:,:,place]
        added_row = np.array([0,0,0,1])
        pose = np.vstack((pose, added_row))
    
        img_transform = Image_Transform(200,200)
        img_mask = img_transform(img)

        label_img = label_bottle_separate(label)
        choose = choose_points(label_img, depth_img, 500)  
        cloud = depth_img_to_pcl(depth_img, label_img, choose, camera_matrix) 
        choose = np.expand_dims(choose, axis = 0)
        bottle_pointcloud = self.xyz_cloud
        target, model_points = target_pointcloud(pose[:3,:], bottle_pointcloud)

        return torch.from_numpy(cloud.astype(np.float32)),\
               torch.LongTensor(choose.astype(np.int32)),\
               img_mask,\
               torch.from_numpy(target.astype(np.float32)),\
               torch.from_numpy(model_points.astype(np.float32)),\
               torch.LongTensor([0])
        
    def __len__(self):
        return len(self.datalist)