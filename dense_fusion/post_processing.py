import torch
import numpy as np


class Weights_Save():

    def __init__(self,optimizer, model, path):
        self.optimizer = optimizer
        self.model = model
        self.path = path
        
    def save_checkpoint(self):

    # Basic details
        checkpoint = {
            'train_data_len': self.model.train_data_len,
            'test_data_len': self.model.test_data_len, 
            'synthetic_data_type': self.model.synthetic_data_type,
            'epochs': self.model.epochs,
            'train_loss': self.model.train_loss,
            'validation_loss': self.model.validation_loss
        }

        checkpoint['cnn'] = self.model.module.cnn   # . module is for the multiple gpu
        checkpoint['feat'] = self.model.module.feat
        checkpoint['state_dict'] = self.model.module.state_dict()
        
        checkpoint['optimizer'] = self.optimizer
        checkpoint['optimizer_state_dict'] = self.optimizer.state_dict()
        
        torch.save(checkpoint, self.path)